import discord
from discord import Embed, File
from discord.ext import commands, tasks
from discord.ext.commands import cooldown, BucketType
import random
import sqlite3

from Utilities import HelperClass
from AccountManager import AccountManager


class OiaLt(commands.Cog):
    def __init__(self, client):
        self.client = client
        self.accountManager = AccountManager(self.client)
        self.helperClass = HelperClass()

    # Current Version: 2.0
    picks = ["Aiko", "Anastasia", "Asmodeus", "Astaroth", "Ava", "Azazel", "Carla", "Clarice", "David", "Dildo Boi",
             "Dojo Owner", "Father Mitchell", "Fit Jack", "Fit Jack's Groupie", "Former Asmodeus", "Funtime", "Hiromi",
             "Hitman Mike", "Iris", "Jasmine", "Jason", "Johnny", "Judie", "Kazuma", "Lauren", "Lilith", "Mayor", "MC",
             "Messy Hair Lauren", "Mike the Exterminator", "Moloch", "Nightmare Demon", "Oliver", "Orochi", "Priestess",
             "Principal", "Rebecca", "Robbie Murray", "Ruth", "Samael", "Shop Girl", "Slaughter Clan", "Spiderman",
             "Stabby Police", "Stone Elephant", "Sun Lovers", "Susanna", "Swimsuit Ruth", "Tom", "Ulric",
             "Yakuza Mike", "Zombie Magnus", "Monster Lilith", "93", "Train Conductor", "Fake Hiromi"]
    cooldowntime = 82800

    async def chooseGF(self, ctx):
        db = sqlite3.connect("main.sqlite")
        cursor = db.cursor()
        discordID = str(ctx.author.id)
        uid = await self.accountManager.getUserID(discordID, cursor)

        seconds = False
        gf = random.choice(self.picks)

        description = f"Congratulations {ctx.author.mention}...? Your companion for the day is {gf}."
        footer = "not too shabby tbf"
        field_name = "Uncategorized..."
        field_value = "A character that doesn't\nbelong into any collection\n(yet?)"
        field2_name = "Effects:"
        field2_value = "None, probably for the best..."

        cursor.execute("SELECT last_gf FROM usersnew WHERE user_id=?", [uid])
        l = cursor.fetchone()
        lastGirl = l[0]

        if lastGirl == gf:
            seconds = True

        cursor.execute("UPDATE usersnew SET last_gf=? WHERE user_id=?", [gf, uid])

        # HAREM
        if gf == "Judie":
            description = f"Congratulations {ctx.author.mention}! Your gf for the day is {gf}!"
            footer = "You really like that pillow, don't you?"
            cursor.execute("SELECT judie FROM oialt_harem WHERE user_id=?", [uid])
            duplicate = cursor.fetchone()

            if duplicate[0] == "NONE":
                cursor.execute("UPDATE oialt_harem SET judie='Judie' WHERE user_id=?", [uid])
                field_name = "Harem"
                field_value = "A wild Judie has appeared in your Harem!"
            else:
                field_name = "Harem"
                field_value = "It seems Judie already is in your harem though... tough luck"
            cursor.execute("UPDATE oialt_harem SET last_li='judie' WHERE user_id=?", [uid])

            if seconds is True:
                field_name = "Respect dude"
                field_value = "Damn Judie twice in a row...\n10/10 man, ggs"
        # HAREM
        elif gf == "Lauren":
            description = f"Congratulations {ctx.author.mention}! Your gf for the day is {gf}!"
            footer = "Hey what's up dumbass?"
            cursor.execute("SELECT lauren FROM oialt_harem WHERE user_id=?", [uid])
            duplicate = cursor.fetchone()
            field2_value = "Will be Orochi's favourite purchase, watch out!"

            if duplicate[0] == "NONE":
                cursor.execute("UPDATE oialt_harem SET lauren='Lauren' WHERE user_id=?", [uid])
                field_name = "Harem"
                field_value = "A wild Lauren has appeared in your Harem!"
            else:
                field_name = "Harem"
                field_value = "It seems Lauren already is in your harem though... tough luck"
            cursor.execute("UPDATE oialt_harem SET last_li='lauren' WHERE user_id=?", [uid])

            if seconds is True:
                field_name = "Respect dude"
                field_value = "Damn Lauren twice in a row...\n10/10 man, ggs"
        # HAREM
        elif gf == "Messy Hair Lauren":
            description = f"Congratulations {ctx.author.mention}! Your gf for the day is {gf}!"
            footer = "lucky you, dude..."
            cursor.execute("SELECT messy_hair_lauren FROM oialt_harem WHERE user_id=?", [uid])
            duplicate = cursor.fetchone()
            field2_value = "Will be Orochi's favourite purchase, watch out!"

            if duplicate[0] == "NONE":
                cursor.execute("UPDATE oialt_harem SET messy_hair_lauren='Messy Hair Lauren' WHERE user_id=?", [uid])
                field_name = "Harem"
                field_value = "A wild Messy Hair Lauren has appeared in your Harem!"
            else:
                field_name = "Harem"
                field_value = "It seems Messy Hair Lauren already is in your harem though... tough luck"
            cursor.execute("UPDATE oialt_harem SET last_li='messy_hair_lauren' WHERE user_id=?", [uid])

            if seconds is True:
                field_name = "Respect dude"
                field_value = "Damn Messy Hair Lauren twice in a row...\nFrigging legend"
        # HAREM
        elif gf == "Carla":
            description = f"Congratulations {ctx.author.mention}! Your gf for the day is {gf}!"
            footer = "You're such a naughty boy... I'm going to have to punish you! Grounded for 2 weeks"
            cursor.execute("SELECT carla FROM oialt_harem WHERE user_id=?", [uid])
            duplicate = cursor.fetchone()

            if duplicate[0] == "NONE":
                cursor.execute("UPDATE oialt_harem SET carla='Carla' WHERE user_id=?", [uid])
                field_name = "Harem"
                field_value = "A wild Carla has appeared in your Harem!"
            else:
                field_name = "Harem"
                field_value = "It seems Carla already is in your harem though... tough luck"
            cursor.execute("UPDATE oialt_harem SET last_li='carla' WHERE user_id=?", [uid])

            if seconds is True:
                field_name = "Respect dude"
                field_value = "Damn Carla twice in a row...\n'I got that MILF money'"
        # HAREM
        elif gf == "Iris":
            description = f"Congratulations {ctx.author.mention}! Your gf for the day is {gf}!"
            footer = "Your brother has a huge dick Judie! Did you know it?"
            cursor.execute("SELECT iris FROM oialt_harem WHERE user_id=?", [uid])
            duplicate = cursor.fetchone()

            if duplicate[0] == "NONE":
                cursor.execute("UPDATE oialt_harem SET iris='Iris' WHERE user_id=?", [uid])
                field_name = "Harem"
                field_value = "A wild Iris has appeared in your Harem!"
            else:
                field_name = "Harem"
                field_value = "It seems Iris already is in your harem though... tough luck"
            cursor.execute("UPDATE oialt_harem SET last_li='iris' WHERE user_id=?", [uid])

            if seconds is True:
                field_name = "Respect dude"
                field_value = "Iris twice in a row...\n'Now I want you to fu€k me in the a$$!'"
        # HAREM & BOI PROTECTOR
        elif gf == "Aiko":
            description = f"Congratulations {ctx.author.mention}! Your gf for the day is {gf}!"
            footer = "Less talking and more fighting..."
            cursor.execute("SELECT aiko FROM oialt_harem WHERE user_id=?", [uid])
            duplicate = cursor.fetchone()

            if duplicate[0] == "NONE":
                cursor.execute("UPDATE oialt_harem SET aiko='Aiko' WHERE user_id=?", [uid])
                field_name = "Harem"
                field_value = "A wild Aiko has appeared in your Harem!"
            else:
                field_name = "Harem"
                field_value = "It seems Aiko already is in your harem though... tough luck"
            cursor.execute("UPDATE oialt_harem SET last_li='aiko' WHERE user_id=?", [uid])

            if seconds is True:
                field_name = "Respect dude"
                field_value = "Aiko twice in a row...\nWatch your windows when you're wanking tho!"

            cursor.execute("SELECT aiko FROM usersnew WHERE user_id=?", [uid])
            protecc = cursor.fetchone()
            if protecc[0] == 0:
                cursor.execute("UPDATE usersnew SET aiko=1 WHERE user_id=?", [uid])
                field2_value = "Aiko protects the boys from Azazel! What a woman!"
            else:
                field2_value = "Sorry buddy, can't stack protections..."
        # HAREM
        elif gf == "Jasmine":
            description = f"Congratulations {ctx.author.mention}! Your gf for the day is {gf}!"
            footer = "Can't stop thinking about your fucking face since the day I threw the party"
            cursor.execute("SELECT jasmine FROM oialt_harem WHERE user_id=?", [uid])
            duplicate = cursor.fetchone()

            if duplicate[0] == "NONE":
                cursor.execute("UPDATE oialt_harem SET jasmine='Jasmine' WHERE user_id=?", [uid])
                field_name = "Harem"
                field_value = "A wild Jasmine has appeared in your Harem!"
            else:
                field_name = "Harem"
                field_value = "It seems Jasmine already is in your harem though... tough luck"
            cursor.execute("UPDATE oialt_harem SET last_li='jasmine' WHERE user_id=?", [uid])

            if seconds is True:
                field_name = "Respect dude"
                field_value = "Jasmine twice in a row...\nPrepare the buttplug :heheboye:"
        # HAREM
        elif gf == "Rebecca":
            description = f"Congratulations {ctx.author.mention}! Your gf for the day is {gf}!"
            footer = "Now I don't look like a respectable teacher, do I?"
            cursor.execute("SELECT rebecca FROM oialt_harem WHERE user_id=?", [uid])
            duplicate = cursor.fetchone()

            if duplicate[0] == "NONE":
                cursor.execute("UPDATE oialt_harem SET rebecca='Rebecca' WHERE user_id=?", [uid])
                field_name = "Harem"
                field_value = "A wild Rebecca has appeared in your Harem!"
            else:
                field_name = "Harem"
                field_value = "It seems Rebecca already is in your harem though... tough luck"
            cursor.execute("UPDATE oialt_harem SET last_li='rebecca' WHERE user_id=?", [uid])

            if seconds is True:
                field_name = "Respect dude"
                field_value = "Rebecca twice in a row...\nYou gotta punish her for being such a bad teacher!"
        # STABBY CLAN
        elif gf == "Yakuza Mike":
            description = f"Congratulations {ctx.author.mention}! Your protector for the day is {gf}!"
            cursor.execute("SELECT yakuza FROM stabby_mikes WHERE user_id=?", [uid])
            footer = "Hahaha the melon!"
            duplicate = cursor.fetchone()

            if duplicate[0] == "NONE":
                cursor.execute("UPDATE stabby_mikes SET yakuza='Yakuza Mike' WHERE user_id=?", [uid])
                field_name = "Stabby Clan"
                field_value = "The new recruit from the stabby clan!"
            else:
                field_name = "Stabby Clan"
                field_value = "It seems Yakuza Mike already is in your clan though... tough luck"
            cursor.execute("UPDATE stabby_mikes SET last_mike='yakuza' WHERE user_id=?", [uid])

            if seconds is True:
                field_name = "Damn bro"
                field_value = "Yakuza Mike twice in a row...\nHope you don't get into shady business deals :PepeSuspicious:"
        # STABBY CLAN
        elif gf == "Stabby Police":
            description = f"Congratulations {ctx.author.mention}! Your protector for the day is {gf}!"
            footer = "Let's give that mayor a scare, shall we?"
            cursor.execute("SELECT police FROM stabby_mikes WHERE user_id=?", [uid])
            duplicate = cursor.fetchone()

            if duplicate[0] == "NONE":
                cursor.execute("UPDATE stabby_mikes SET police='Stabby Police' WHERE user_id=?", [uid])
                field_name = "Stabby Clan"
                field_value = "The new recruit from the stabby clan!"
            else:
                field_name = "Stabby Clan"
                field_value = "It seems Stabby Police already is in your clan though... tough luck"
            cursor.execute("UPDATE stabby_mikes SET last_mike='police' WHERE user_id=?", [uid])

            if seconds is True:
                field_name = "Damn bro"
                field_value = "Stabby Police twice in a row...\nI think your town is free of shady mayors now mate!"
        # STABBY CLAN
        elif gf == "Mike the Exterminator":
            description = f"Congratulations {ctx.author.mention}! Your protector for the day is {gf}!"
            footer = "feel safe already!"
            cursor.execute("SELECT exterminator FROM stabby_mikes WHERE user_id=?", [uid])
            duplicate = cursor.fetchone()

            if duplicate[0] == "NONE":
                cursor.execute("UPDATE stabby_mikes SET exterminator='Mike the Exterminator' WHERE user_id=?", [uid])
                field_name = "Stabby Clan"
                field_value = "The new recruit from the stabby clan!"
            else:
                field_name = "Stabby Clan"
                field_value = "It seems Mike the Exterminator already is in your clan though... tough luck"
            cursor.execute("UPDATE stabby_mikes SET last_mike='exterminator' WHERE user_id=?", [uid])

            if seconds is True:
                field_name = "Damn bro"
                field_value = "the Exterminator twice in a row...\nMake sure you're clear of gangsters..." \
                              "for their own sake"
        # STABBY CLAN
        elif gf == "Hitman Mike":
            description = f"Congratulations {ctx.author.mention}! Your protector for the day is {gf}!"
            footer = "Holy Shit!"
            cursor.execute("SELECT hitman FROM stabby_mikes WHERE user_id=?", [uid])
            duplicate = cursor.fetchone()

            if duplicate[0] == "NONE":
                cursor.execute("UPDATE stabby_mikes SET hitman='Hitman Mike' WHERE user_id=?", [uid])
                field_name = "Stabby Clan"
                field_value = "The new recruit from the stabby clan!"
            else:
                field_name = "Stabby Clan"
                field_value = "It seems Hitman Mike already is in your clan though... tough luck"
            cursor.execute("UPDATE stabby_mikes SET last_mike='hitman' WHERE user_id=?", [uid])

            if seconds is True:
                field_name = "Damn bro"
                field_value = "Hitman Mike twice in a row...\nI think your town is free of shady mayors now mate!"
        # STABBY CLAN
        elif gf == "Father Mitchell":
            description = f"Congratulations {ctx.author.mention}! Your protector for the day is {gf}!"
            footer = "REPENT!"
            cursor.execute("SELECT priest FROM stabby_mikes WHERE user_id=?", [uid])
            duplicate = cursor.fetchone()
            field2_value = "preferred target for Astaroth, be careful..."

            if duplicate[0] == "NONE":
                cursor.execute("UPDATE stabby_mikes SET priest='Father Mitchell' WHERE user_id=?", [uid])
                field_name = "Stabby Clan"
                field_value = "The new recruit from the stabby clan!"
            else:
                field_name = "Stabby Clan"
                field_value = "It seems Father Mitchell already is in your clan though... tough luck"
            cursor.execute("UPDATE stabby_mikes SET last_mike='priest' WHERE user_id=?", [uid])

            if seconds is True:
                field_name = "Damn bro"
                field_value = "Father Mitchell twice in a row...\nFree ticket to Heaven. Amen"
        # STABBY CLAN
        elif gf == "Anastasia":
            description = f"Congratulations {ctx.author.mention}! Your gf for the day is {gf}!"
            footer = "What a fine lady"
            cursor.execute("SELECT anastasia FROM stabby_mikes WHERE user_id=?", [uid])
            duplicate = cursor.fetchone()

            if duplicate[0] == "NONE":
                cursor.execute("UPDATE stabby_mikes SET anastasia='Anastasia' WHERE user_id=?", [uid])
                field_name = "Stabby Clan"
                field_value = "The new recruit from the stabby clan!"
            else:
                field_name = "Stabby Clan"
                field_value = "It seems Anastasia already is in your clan though... tough luck"
            cursor.execute("UPDATE stabby_mikes SET last_mike='anastasia' WHERE user_id=?", [uid])

            if seconds is True:
                field_name = "Risky Business tho"
                field_value = "Anastasia twice in a row...\nGoing for seconds with a Russian business woman? I'd " \
                              "reconsider for any other woman of that kind"
        # THE BOYS & MIKE PROTECTOR
        elif gf == "MC":
            description = f"Congratulations {ctx.author.mention}! Your protector for the day is {gf}!"
            footer = "Holy shit I look so evil in this picture."
            cursor.execute("SELECT mc FROM the_boys WHERE user_id=?", [uid])
            duplicate = cursor.fetchone()

            if duplicate[0] == "NONE":
                cursor.execute("UPDATE the_boys SET mc='MC' WHERE user_id=?", [uid])
                field_name = "The Boys"
                field_value = "MC just joined the boys! Let's effing goo"
            else:
                field_name = "The boys"
                field_value = "It seems MC is already one of the boys though... tough luck"
            cursor.execute("UPDATE the_boys SET last_boi='mc' WHERE user_id=?", [uid])

            if seconds is True:
                field_name = "Noice"
                field_value = "MC twice in a row...\nMake sure you don't get Olivered!"

            cursor.execute("SELECT mc FROM usersnew WHERE user_id=?", [uid])
            protecc = cursor.fetchone()

            if protecc[0] == 0:
                cursor.execute("UPDATE usersnew SET mc=1 WHERE user_id=?", [uid])
                field2_value = "This man protects your stabby clan from the next Astaroth! If that's not cool..."
            else:
                field2_value = "Sorry buddy, can't stack protections..."
        # THE BOYS
        elif gf == "Tom":
            description = f"Congratulations {ctx.author.mention}! Your protector for the day is {gf}!"
            footer = "feel safe already!"
            cursor.execute("SELECT tom FROM the_boys WHERE user_id=?", [uid])
            duplicate = cursor.fetchone()

            if duplicate[0] == "NONE":
                cursor.execute("UPDATE the_boys SET tom='Tom' WHERE user_id=?", [uid])
                field_name = "The Boys"
                field_value = "Tom just joined the boys! Let's effing goo"
            else:
                field_name = "The boys"
                field_value = "It seems Tom is already one of the boys though... tough luck"
            cursor.execute("UPDATE the_boys SET last_boi='tom' WHERE user_id=?", [uid])

            if seconds is True:
                field_name = "Damn bro"
                field_value = "Tom twice in a row...\nHopefully the spray isn't only temporary *sweating*"
        # THE BOYS
        elif gf == "Oliver":
            description = f"Congratulations (?) {ctx.author.mention}! Your buddy for the day is {gf}!"
            footer = "I was just inviting her over to my Equestrian property for the weekend"
            cursor.execute("SELECT oliver FROM the_boys WHERE user_id=?", [uid])
            duplicate = cursor.fetchone()

            if duplicate[0] == "NONE":
                cursor.execute("UPDATE the_boys SET oliver='Oliver' WHERE user_id=?", [uid])
                field_name = "The Boys"
                field_value = "Oliver just joined the boys! Let's effing goo"
            else:
                field_name = "The boys"
                field_value = "It seems Oliver is already one of the boys though... tough luck"
            cursor.execute("UPDATE the_boys SET last_boi='oliver' WHERE user_id=?", [uid])

            if seconds is True:
                field_name = "Weird choice man"
                field_value = "Oliver twice in a row...\nAre you guys teaming up on Ruth??"
        # THE BOYS
        elif gf == "Fit Jack":
            description = f"Congratulations {ctx.author.mention}! Your protector for the day is {gf}!"
            footer = "feel safe already!"
            cursor.execute("SELECT fit_jack FROM the_boys WHERE user_id=?", [uid])
            duplicate = cursor.fetchone()

            if duplicate[0] == "NONE":
                cursor.execute("UPDATE the_boys SET fit_jack='Fit Jack' WHERE user_id=?", [uid])
                field_name = "The Boys"
                field_value = "Fit Jack just joined the boys! Let's effing goo"
            else:
                field_name = "The boys"
                field_value = "It seems Fit Jack is already one of the boys though... tough luck"
            cursor.execute("UPDATE the_boys SET last_boi='fit_jack' WHERE user_id=?", [uid])

            if seconds is True:
                field_name = "Damn bro"
                field_value = "Fit Jack twice in a row...\nYou guys gym buddies now? Going for the GAINZ bro"
        # THE BOYS
        elif gf == "Asmodeus":
            description = f"Congratulations {ctx.author.mention}! Your protector for the day is {gf}!"
            footer = "feel safe already!"
            cursor.execute("SELECT asmodeus FROM the_boys WHERE user_id=?", [uid])
            duplicate = cursor.fetchone()

            if duplicate[0] == "NONE":
                cursor.execute("UPDATE the_boys SET asmodeus='Asmodeus' WHERE user_id=?", [uid])
                field_name = "The Boys"
                field_value = "Asmodeus just joined the boys! Let's effing goo"
            else:
                field_name = "The boys"
                field_value = "It seems Asmodeus is already one of the boys though... tough luck"
            cursor.execute("UPDATE the_boys SET last_boi='asmodeus' WHERE user_id=?", [uid])

            if seconds is True:
                field_name = "Seems chill man"
                field_value = "Asmodeus twice in a row...\nMust be chill man, enjoy!"
        # HAREM BUYER
        elif gf == "Orochi":
            description = f"Yikes! Your companion for the day is {gf}. Good luck {ctx.author.mention}! (You'll need it...)"
            footer = "YIKES"
            cursor.execute("SELECT funtime FROM usersnew WHERE user_id=?", [uid])
            protection = cursor.fetchone()

            if protection[0] == 0:
                field_name = "OH NO"
                field_value = "Orochi offers you a deal you can't refuse..."
                field2_name = "I want your henchwoman!"
                footer = "How could you...?"

                cursor.execute("SELECT lauren FROM oialt_harem WHERE user_id=?", [uid])
                lauren = cursor.fetchone()

                if lauren[0] == 'Lauren':
                    field2_value = "Orochi bought Lauren out of your harem. How could you sell her??"
                    cursor.execute("UPDATE oialt_harem SET lauren='NONE' WHERE user_id=?", [uid])
                else:
                    cursor.execute("SELECT messy_hair_lauren FROM oialt_harem WHERE user_id=?", [uid])
                    mhlauren = cursor.fetchone()
                    if mhlauren[0] == 'Messy Hair Lauren':
                        field2_value = "Orochi bought Messy Hair Lauren out of your harem. How could you sell her??"
                        cursor.execute("UPDATE oialt_harem SET messy_hair_lauren='NONE' WHERE user_id=?", [uid])
                    else:
                        cursor.execute("SELECT last_li FROM oialt_harem WHERE user_id=?", [uid])
                        lastgf = cursor.fetchone()
                        if lastgf[0] == 'judie':
                            field2_value = "Orochi bought Judie out of your harem. How could you sell her??"
                            cursor.execute("UPDATE oialt_harem SET judie='NONE' WHERE user_id=?", [uid])

                        elif lastgf[0] == 'lauren':
                            field2_value = "Hah, true, I already bought Lauren last time..."

                        elif lastgf[0] == 'messy_hair_lauren':
                            field2_value = "Hah, true, I already bought Messy Hair Lauren last time..."

                        elif lastgf[0] == 'carla':
                            field2_value = "Orochi bought Carla out of your harem. How could you sell her??"
                            cursor.execute("UPDATE oialt_harem SET carla='NONE' WHERE user_id=?", [uid])

                        elif lastgf[0] == 'iris':
                            field2_value = "Orochi bought Iris out of your harem. How could you sell her??"
                            cursor.execute("UPDATE oialt_harem SET iris='NONE' WHERE user_id=?", [uid])

                        elif lastgf[0] == 'aiko':
                            field2_value = "Orochi bought Aiko out of your harem. How could you sell her??"
                            cursor.execute("UPDATE oialt_harem SET aiko='NONE' WHERE user_id=?", [uid])

                        elif lastgf[0] == 'jasmine':
                            field2_value = "Orochi bought Jasmine out of your harem. How could you sell her??"
                            cursor.execute("UPDATE oialt_harem SET jasmine='NONE' WHERE user_id=?", [uid])

                        elif lastgf[0] == 'rebecca':
                            field2_value = "Orochi bought Rebecca out of your harem. How could you sell her??"
                            cursor.execute("UPDATE oialt_harem SET rebecca='NONE' WHERE user_id=?", [uid])

                        else:
                            field2_name = "You don't have a gf?"
                            field2_value = "HAH you don't even get some lmao, see you next time!"
                            footer = "a real Chad"

                    cursor.execute("UPDATE oialt_harem SET last_li='NONE' WHERE user_id=?", [uid])

            else:
                cursor.execute("UPDATE usersnew SET funtime=0 WHERE user_id=?", [uid])
                field_name = "That was close"
                field_value = "GODDAMN IT, WHY AREN'T YOU LAUGHING OROCHI??!"
                field2_name = "I'll be back..."
                field2_value = "Your buddy from the funtime clan got your back... this time"

            if seconds is True:
                field_name = "WTF Man"
                field_value = "Orochi twice in a row...?\nFr man? two girls for the price of one or what's the deal here?"
        # HAREM PROTECTOR
        elif gf == "Funtime":
            description = f"Hey nice! Your companion for the day is {gf}! Have fun {ctx.author.mention}!"
            footer = "And the the Spaniard says: 'I didn't mean to interrupt, but that's not the melon!'"
            cursor.execute("SELECT funtime FROM usersnew WHERE user_id=?", [uid])
            protecc = cursor.fetchone()

            if protecc[0] == 0:
                cursor.execute("UPDATE usersnew SET funtime=1 WHERE user_id=?", [uid])
                field2_value = "Protects your harem from the next Orochi. Isn't that cool?"
            else:
                field_value = "Sorry buddy, can't stack protections..."

            if seconds is True:
                field_name = "Seems chill man"
                field_value = "Funtime twice in a row...\nMust be chill man, enjoy!"
        # MIKE KILLER
        elif gf == "Astaroth":
            description = f"Yikes! Your companion for the day is {gf}. Good luck {ctx.author.mention}! (You'll need it...)"
            cursor.execute("SELECT mc FROM usersnew WHERE user_id=?", [uid])
            protection = cursor.fetchone()

            if protection[0] == 0:
                field_name = "OH SHIT"
                field_value = "Astaroth pulled out a gun!"
                field2_name = "Did you think you could defeat me with such an amateur tactic?"
                footer = "brought a knife to a gunfight..."
                cursor.execute("SELECT priest FROM stabby_mikes WHERE user_id=?", [uid])
                priest = cursor.fetchone()
                if priest[0] == 'Father Mitchell':
                    field2_value = "Astaroth shot Father Mitchell straight in the heart! R.I.P."
                    cursor.execute("UPDATE stabby_mikes SET priest='NONE' WHERE user_id=?", [uid])
                else:
                    cursor.execute("SELECT last_mike FROM stabby_mikes WHERE user_id=?", [uid])
                    mike = cursor.fetchone()

                    if mike[0] == "priest":
                        field2_value = "Your hitman is already dead, dumbass."

                    elif mike[0] == "yakuza":
                        field2_value = "Astaroth shot Yakuza Mike in the forehead! R.I.P."
                        cursor.execute("UPDATE stabby_mikes SET yakuza='NONE' WHERE user_id=?", [uid])

                    elif mike[0] == "police":
                        field2_value = "Astaroth decimated the Stabby Police unit! R.I.P."
                        cursor.execute("UPDATE stabby_mikes SET police='NONE' WHERE user_id=?", [uid])

                    elif mike[0] == "exterminator":
                        field2_value = "Astaroth shot Mike the Exterminator in neck! R.I.P."
                        cursor.execute("UPDATE stabby_mikes SET exterminator='NONE' WHERE user_id=?", [uid])

                    elif mike[0] == "hitman":
                        field2_value = "Astaroth shot Mike the Hitman in the heart! R.I.P."
                        cursor.execute("UPDATE stabby_mikes SET hitman='NONE' WHERE user_id=?", [uid])

                    elif mike[0] == "anastasia":
                        field2_value = "Astaroth shot Anastasia! How low to fire at an honourable lady..."
                        cursor.execute("UPDATE stabby_mikes SET anastasia='NONE' WHERE user_id=?", [uid])

                    else:
                        field2_name = "Must've been a shadow then..."
                        field2_value = "I'll be waiting for you where it all started."
                        footer = "close one..."

                cursor.execute("UPDATE stabby_mikes SET last_mike='NONE' WHERE user_id=?", [uid])
            else:
                cursor.execute("UPDATE usersnew SET mc=0 WHERE user_id=?", [uid])
                field_name = "That was close..."
                field_value = "If you die, you die"
                field2_name = "So this is how it feels to possess the power of the Grimoire..."
                field2_value = "MC saved the clan... what a man!"
                footer = "close call..."

            if seconds is True:
                field_name = "FR man?"
                field_value = "Astaroth twice in a row...\nHave you joined the dark side? :cry:"

        elif gf == "Zombie Magnus":
            description = f"Yikes! Your companion for the day is {gf}. Good luck {ctx.author.mention}! (You'll need it...)"
            footer = "YIKES"

            if seconds is True:
                field_name = "Nah bro"
                field_value = "Zombie Magnus twice in a row...\nAt this point you have a deathwish mate"

        elif gf in ("Swimsuit Ruth", "Ruth"):
            description = f"Yikes! Your gf for the day is {gf}. Good luck {ctx.author.mention}! (You'll need it...)"
            footer = "I'll take the elephant, thank you..."

            if seconds is True:
                field_name = "Bruh"
                field_value = "Ruth twice in a row...\nStay strong king, police are on their way to save you " \
                              "(or maybe Oliver)"
        # POTENTIAL LI MUTATOR
        elif gf == "Monster Lilith":
            description = f"Yikes! Your gf for the day is {gf}. Good luck {ctx.author.mention}! (You'll need it...)"
            cursor.execute("SELECT nine_three FROM usersnew WHERE user_id=?", [uid])
            protection = cursor.fetchone()

            if protection[0] == 0:
                field_name = "OH SHIT"
                field_value = "A wild Monster Lilith has appeared"
                field2_name = "Monster Lilith uses 'research chemical'! It's super effective"
                footer = "Science, bitch!"
                cursor.execute("SELECT lilith FROM li_potential WHERE user_id=?", [uid])
                guineapig = cursor.fetchone()
                if guineapig[0] == 'Lilith':
                    field2_value = "Lilith just mutated to a living set of spare ribs! YIKES"
                    cursor.execute("UPDATE li_potential SET lilith='NONE' WHERE user_id=?", [uid])
                else:
                    cursor.execute("SELECT last_potential_li FROM li_potential WHERE user_id=?", [uid])
                    guineapig = cursor.fetchone()

                    if guineapig[0] == "ava":
                        field2_value = "Ava just mutated to a leader price version of Big Chungus! YIKES"
                        cursor.execute("UPDATE li_potential SET ava='NONE' WHERE user_id=?", [uid])

                    elif guineapig[0] == "fit_jack_groupie":
                        field2_value = "Fit Jack's groupie's muscles just expanded to the point she can't move! YIKES"
                        cursor.execute("UPDATE li_potential SET fit_jack_groupie='NONE' WHERE user_id=?", [uid])

                    elif guineapig[0] == "priestess":
                        field2_value = "The priestess mutated into a zombified nun! YIKES"
                        cursor.execute("UPDATE li_potential SET train_conductor='NONE' WHERE user_id=?", [uid])

                    elif guineapig[0] == "shop_girl":
                        field2_value = "The shop girl's skin just turned to papyrus and fell off! YIKES"
                        cursor.execute("UPDATE li_potential SET shop_girl='NONE' WHERE user_id=?", [uid])

                    elif guineapig[0] == "stone_elephant":
                        field2_value = "The stone elephant stood up and fled from you! YIKES\n*how rude tho*"
                        cursor.execute("UPDATE li_potential SET stone_elephant='NONE' WHERE user_id=?", [uid])

                    else:
                        field2_name = "Must've been a shadow then..."
                        field2_value = "I'll be waiting for you where it all started."
                        footer = "close one..."
                cursor.execute("UPDATE li_potential SET last_potential_li='NONE' WHERE user_id=?", [uid])
            else:
                cursor.execute("UPDATE usersnew SET nine_three=0 WHERE user_id=?", [uid])
                field_name = "That was close..."
                field_value = "93 beat the ashen lady, yes! hAahAhaHaHa"
                field2_name = "She is doomed now, yes she is"
                field2_value = "93 distracted Monster Lilith for you... for now"
                footer = "close call..."

            if seconds is True:
                field_name = "Hmm yeah no"
                field_value = "Lilith twice in a row...\nHuman form heck yeah but a bone monster? U a master simp man"

        elif gf == "Dildo Boi":
            description = f"Yikes! Your companion for the day is {gf}. Good luck {ctx.author.mention}! (You'll need it...)"
            footer = "*Please don't use me as a dildo...*"

            if seconds is True:
                field_name = "Are you good mate?"
                field_value = "Going for seconds with Dildo Boi? You can get him a dildo to spare your butt a hard time bro"
        # LI POTENTIAL
        elif gf == "Stone Elephant":
            description = f"umm... yeah, your companion for the day is {gf}... damn you must be desperate af " \
                          f"{ctx.author.mention}"
            footer = "Better than the alternative tbf"
            cursor.execute("SELECT stone_elephant FROM li_potential WHERE user_id=?", [uid])
            duplicate = cursor.fetchone()

            if duplicate[0] == "NONE":
                cursor.execute("UPDATE li_potential SET stone_elephant='Stone Elephant' WHERE user_id=?", [uid])
                field_name = "LI Potential"
                field_value = "The Stone Elephant has joined the gang! Might consider asking her out? :wink:"
            else:
                field_name = "LI Potential"
                field_value = "It seems the stone elephant is already part of the gang. Why don't you ask it out tho?"
            cursor.execute("UPDATE li_potential SET last_potential_li='stone_elephant' WHERE user_id=?", [uid])

            if seconds is True:
                field_name = "Can't blame you dude"
                field_value = "The elephant twice in a row...\nBetter than the alternative ngl, wish you luck with " \
                              "the girls now..."

        elif gf == "Sun Lovers":
            description = f"Your companions for the day are the {gf}. Enjoy the sunshine {ctx.author.mention}!"
            footer = "It's a beautiful, sunny day..."

            if seconds is True:
                field_name = "Seems chill man"
                field_value = "Sun Lovers twice in a row...\nPrison Loyalty is important, I understand dude..."

        elif gf == "Dojo Owner":
            description = f"umm... Your companion for the day is {gf}. Don't dishonour the dojo {ctx.author.mention}!"
            footer = "Great company I guess"

            if seconds is True:
                field_name = "Go for it king"
                field_value = "The dojo owner twice in a row...\nGood luck with that black belt man!"

        elif gf == "Former Asmodeus":
            description = f"Congrats {ctx.author.mention}...? Your cellmate today is {gf}"
            footer = "Idk man, seems a little crazy but it's your choice"

            if seconds is True:
                field_name = "Nah bro"
                field_value = "former Asmodeus twice in a row...?\nYou failed man, got the wrong ending."

        elif gf == "Slaughter Clan":
            description = f"Congrats {ctx.author.mention}! Your companion for the day is {gf}"
            footer = "Good luck with the baking business!"

            if seconds is True:
                field_name = "Seems chill man"
                field_value = "Slaughter Clan twice in a row...\nHow's the bakery doing bro?"

        elif gf == "David":
            description = f"Yikes! Your companion for the day is {gf}. I'm sorry for you, {ctx.author.mention}..."
            footer = "WoRk hARd yOunG MAn"

            if seconds is True:
                field_name = "Bruh"
                field_value = "David twice in a row...\nCan you stand that dude? Seems like the uncle who won't stfu " \
                              "at a wedding smh!"

        elif gf == "Priestess":
            description = f"Congrats {ctx.author.mention}! Your gf for the day is {gf}"
            footer = "Blue balls for you today I'm afraid dude"

            if seconds is True:
                field_name = "Legit tbf"
                field_value = "The priestess twice in a row...\nYou really DO want to see into your future do you?"

        elif gf == "Robbie Murray":
            description = f"Congrats {ctx.author.mention}...? Your companion for the day is {gf}"
            footer = "fucking priceless"

            if seconds is True:
                field_name = "Lmao"
                field_value = "Robbie Murray twice in a row...\nDid you lose the 1st time? :KEKW:"

        elif gf == "Moloch":
            description = f"Yikes! Your companion for the day is {gf}. Good luck {ctx.author.mention}! (You'll need it...)"
            footer = "Anyone heard of a Peter from Birmingham?"

            if seconds is True:
                field_name = "Hmm"
                field_value = "Moloch twice in a row...\nSucking up to Jasmine are you? :PepeSuspicious:"

        elif gf == "Kazuma":
            description = f"Congrats {ctx.author.mention}...? Your companion for the day is {gf}!"
            footer = "Free haircut :heheboye:"

            if seconds is True:
                field_name = "A hairdresser? Really?"
                field_value = "Kazuma twice in a row...\nHe cut your hair yesterday mate!"

        elif gf == "Susanna":
            description = f"hey, congrats {ctx.author.mention}! Your gf for the day is {gf}"
            footer = "What a sweet person she is..."

            if seconds is True:
                field_name = "Seems chill man"
                field_value = "Susanna twice in a row...\nI'm sure she appreciates the help in the hotel man, good job!"

        elif gf == "Samael":
            description = f"Yikes! Your companion for the day is {gf}. Good luck {ctx.author.mention}! (You'll need it...)"
            footer = "NOPE, hey Siri turn back time please!"

            if seconds is True:
                field_name = "F"
                field_value = "Samael twice in a row...\nDamn bro, you're dead, you know that right?"

        elif gf == "Principal":
            description = f"Yikes! Your companion for the day is {gf}. Good luck {ctx.author.mention}! (You'll need it...)"
            footer = "Oh you were talking history? Name every medieval Kingdom in England!"

            if seconds is True:
                field_name = "Ain't gonna work bro"
                field_value = "The principal twice in a row?\nYou know he doesn't grade your tests, right?"

        elif gf == "Jason":
            description = f"Yikes! Your companion for the day is {gf}. Good luck {ctx.author.mention}! (You'll need it...)"
            footer = "Hey Tom, your cousin called me a dick!"

            if seconds is True:
                field_name = "..."
                field_value = "Jason twice in a row...\nYou have no friends dude, move on."

        elif gf == "Mayor":
            description = f"Yikes! Your companion for the day is the {gf}. Good luck {ctx.author.mention}! " \
                          f"(You'll need it...)"
            footer = "'I'm not an underage girl, get away from me!'"

            if seconds is True:
                field_name = "AYO"
                field_value = "The mayor twice in a row???\nEnjoy prison for CP dude! You'd deserve it... yikes"
        # THE BOYS
        elif gf == "Hiromi":
            description = f"Congrats, {ctx.author.mention}! Your companion for the day is {gf}!"
            footer = "Ahh, I'm the man you're looking for. What do you need? Coke?"
            cursor.execute("SELECT hiromi FROM the_boys WHERE user_id=?", [uid])
            duplicate = cursor.fetchone()
            if duplicate[0] == 'NONE':
                cursor.execute("UPDATE the_boys SET hiromi='Hiromi' WHERE user_id=?", [uid])
                field_name = "The Boys"
                field_value = "Hiromi just joined the boys! Let's effing goo"
            else:
                field_name = "The boys"
                field_value = "It seems Hiromi is already one of the boys though... tough luck"
            cursor.execute("UPDATE the_boys SET last_boi='hiromi' WHERE user_id=?", [uid])

            if seconds is True:
                field_name = "Seems chill man"
                field_value = "Hiromi twice in a row...\nMust be chill man, enjoy!"

        elif gf == "Nightmare Demon":
            description = f"Yikes! Your companion for the day is {gf}. Good luck {ctx.author.mention}! (You'll need it...)"
            footer = "sorry buddy, can't help you on this one, you gotta run *faints*"

            if seconds is True:
                field_name = "You have LOST your mind bruv"
                field_value = "The nightmare demon? Really?\nEnjoy insomnia dude."

        elif gf == "Clarice":
            description = f"Congrats, {ctx.author.mention}...? Your companion for the day is {gf}!"
            footer = "'But I said that under the seal of confession!'"

            if seconds is True:
                field_name = ":PepeHmmm"
                field_value = "Clarice twice in a row...\nI'd worry about Mikey finding out..."

        elif gf == "Spiderman":
            name = str(ctx.author)
            description = f"Congrats, {ctx.author.mention}! Your companion for the day is {gf}!"
            footer = f"Hey, is there a {name[:-5]}? I have a pizza for {name[:-5]}"

            if seconds is True:
                field_name = "Seems chill man"
                field_value = "Spiderman twice in a row...\nWho doesn't enjoy a good old pizza :kermitYes:"

        elif gf == "Ulric":
            description = f"umm, yeah, congrats I guess {ctx.author.mention}? Your companion for the day is {gf}!"
            footer = "You haven't even bought one of my wines dude!"

            if seconds is True:
                field_name = "You need rehab"
                field_value = "Ulric twice in a row...\nBe careful on that angel's breath man."
        # LI POTENTIAL
        elif gf == "Ava":
            description = f"Congrats, {ctx.author.mention}! Your companion for the day is {gf}!"
            footer = ":HotBale:"
            cursor.execute("SELECT ava FROM li_potential WHERE user_id=?", [uid])
            duplicate = cursor.fetchone()

            if duplicate[0] == "NONE":
                cursor.execute("UPDATE li_potential SET ava='Ava' WHERE user_id=?", [uid])
                field_name = "LI Potential"
                field_value = "Ava has joined the gang! Might consider asking her out? :wink:"
            else:
                field_name = "LI Potential"
                field_value = "It seems Ava is already part of the gang. Why don't you ask her out tho?"
            cursor.execute("UPDATE li_potential SET last_potential_li='ava' WHERE user_id=?", [uid])

            if seconds is True:
                field_name = "Oh boy"
                field_value = "Ava twice in a row...\nGG WP man, enjoy the good time :heheboye:"

        elif gf == "Johnny":
            description = f"Congrats, {ctx.author.mention}! Your protector for the day is {gf}!"
            footer = "His dad is such a drama smh my head"

            if seconds is True:
                field_name = "U good bro?"
                field_value = "Johnny twice in a row...\n... I have nothing to add ngl"
        # BOI KILLER
        elif gf == "Azazel":
            description = f"Yikes! Your companion for the day is {gf}. Good luck {ctx.author.mention}! (You'll need it...)"
            cursor.execute("SELECT aiko FROM usersnew WHERE user_id=?", [uid])
            protection = cursor.fetchone()

            if protection[0] == 0:
                field_name = "OH SHIT"
                field_value = "Azazel pulled out a gun!"
                field2_name = "I don't need you anymore, then. I'll see you in hell."
                footer = "Why did you tell him tho?"
                cursor.execute("SELECT mc FROM the_boys WHERE user_id=?", [uid])
                mc = cursor.fetchone()
                if mc[0] == 'MC':
                    field2_value = "Azazel put MC to sleep forever! R.I.P."
                    cursor.execute("UPDATE the_boys SET mc='NONE' WHERE user_id=?", [uid])

                else:
                    cursor.execute("SELECT last_boi FROM the_boys WHERE user_id=?", [uid])
                    victim = cursor.fetchone()
                    if victim[0] == "mc":
                        field2_value = "I already killed that dude last time!"

                    elif victim[0] == "tom":
                        field2_value = "Azazel put Tom to sleep forever! R.I.P."
                        cursor.execute("UPDATE the_boys SET tom='NONE' WHERE user_id=?", [uid])

                    elif victim[0] == "oliver":
                        field2_value = "Azazel put Oliver to sleep forever! R.I.P."
                        cursor.execute("UPDATE the_boys SET oliver='NONE' WHERE user_id=?", [uid])

                    elif victim[0] == "fit_jack":
                        field2_value = "Azazel put Fit Jack to sleep forever (How did he do it though??)! R.I.P."
                        cursor.execute("UPDATE the_boys SET fit_jack='NONE' WHERE user_id=?", [uid])

                    elif victim[0] == "asmodeus":
                        field2_value = "Azazel put Asmodeus to sleep forever! R.I.P."
                        cursor.execute("UPDATE the_boys SET asmodeus='NONE' WHERE user_id=?", [uid])

                    elif victim[0] == "hiromi":
                        field2_value = "Azazel put Hiromi to sleep forever! R.I.P."
                        cursor.execute("UPDATE the_boys SET hiromi='NONE' WHERE user_id=?", [uid])

                    else:
                        field2_name = "Damn I thought he knew something..."
                        field2_value = "Aight, see you next time kid"
                        footer = "close one..."
                    cursor.execute("UPDATE the_boys SET last_boi='NONE' WHERE user_id=?", [uid])
            else:
                cursor.execute("UPDATE usersnew SET aiko=0 WHERE user_id=?", [uid])
                field_name = "That was close..."
                field_value = "Watch your back buddy"
                field2_name = "You told the truth?"
                field2_value = "Wow! Aiko stepped in to save the boys! Grant this woman council protection!"
                footer = "that bastard lied to me..."

            if seconds is True:
                field_name = ":disappointed~1:"
                field_value = "Azazel twice in a row...\nCan't expect Aiko to save your ass everytime man, come on!"
        # LI POTENTIAL
        elif gf == "Shop Girl":
            un = str(ctx.author)
            if un == "Frost#0169":
                field2_name = "You can call me daddy..."
                field2_value = "Well, well, well... if this were real life, a creator-createe relationship wouldn't be so acceptable :PepeSuspicious:"
            description = f"Congrats, {ctx.author.mention}! Your gf for the day is {gf}!"
            footer = "Yeah why not..."
            cursor.execute("SELECT shop_girl FROM li_potential WHERE user_id=?", [uid])
            duplicate = cursor.fetchone()

            if duplicate[0] == "NONE":
                cursor.execute("UPDATE li_potential SET shop_girl='Shop Girl' WHERE user_id=?", [uid])
                field_name = "LI Potential"
                field_value = "The Shop Girl has joined the gang! Might consider asking her out? :wink:"
            else:
                field_name = "LI Potential"
                field_value = "It seems the Shop Girl is already part of the gang. Why don't you ask her out tho?"
            cursor.execute("UPDATE li_potential SET last_potential_li='shop_girl' WHERE user_id=?", [uid])

            if seconds is True:
                field_name = "Damn man"
                field_value = "the Shop Girl twice in a row...\nYou have balls to ask out a saleswoman at work!"
        # LI POTENTIAL
        elif gf == "Fit Jack's Groupie":
            description = f"Congrats, {ctx.author.mention}! Your protector for the day is {gf}!"
            footer = "pure dedication"
            cursor.execute("SELECT fit_jack_groupie FROM li_potential WHERE user_id=?", [uid])
            duplicate = cursor.fetchone()

            if duplicate[0] == "NONE":
                cursor.execute("UPDATE li_potential SET fit_jack_groupie='Fit Jack'' s Groupie' WHERE user_id=?",
                               [uid])
                field_name = "LI Potential"
                field_value = "Fit Jack's Groupie has joined the gang! Might consider asking her out? :wink:"
            else:
                field_name = "LI Potential"
                field_value = "It seems Fit Jack's Groupie is already part of the gang. Why don't you ask her out tho?"
            cursor.execute("UPDATE li_potential SET last_potential_li='fit_jack_groupie' WHERE user_id=?", [uid])

            if seconds is True:
                field_name = "Need a break?"
                field_value = "Jack's groupie twice in a row...\nDamn you must be running 24/7. Respect bro"
        # LI POTENTIAL
        elif gf == "Lilith":
            description = f"congrats, {ctx.author.mention}! Your date for the day is {gf}!"
            footer = "Not too shabby..."
            cursor.execute("SELECT lilith FROM li_potential WHERE user_id=?", [uid])
            duplicate = cursor.fetchone()
            if duplicate[0] == "NONE":
                cursor.execute("UPDATE li_potential SET lilith='Lilith' WHERE user_id=?", [uid])
                field_name = "LI Potential"
                field_value = "Lilith has joined the gang! Might consider asking her out? :wink:"
            else:
                field_name = "LI Potential"
                field_value = "It seems Lilith is already part of the gang. Why don't you ask her out tho?"
            cursor.execute("UPDATE li_potential SET last_potential_li='lilith' WHERE user_id=?", [uid])

            if seconds is True:
                field_name = "Big Brain"
                field_value = "Lilith twice in a row...\nName every element of the periodic table. You should know by now!"
        # POTENTIAL LI PROTECTOR
        elif gf == "93":
            description = f"Congrats, {ctx.author.mention}? Your protector for the day is {gf}!"
            footer = "Back off, you look scary dude..."
            cursor.execute("SELECT nine_three FROM usersnew WHERE user_id=?", [uid])
            protecc = cursor.fetchone()

            if protecc[0] == 0:
                cursor.execute("UPDATE usersnew SET nine_three=1 WHERE user_id=?", [uid])
                field2_value = "Protects your potential LI's from the next Monster Lilith. Isn't that cool?"
            else:
                field_value = "Sorry buddy, can't stack protections..."

            if seconds is True:
                field_name = "U good bro?"
                field_value = "93 twice in a row...\nAre you **that** into horror?"
        # LI POTENTIAL
        elif gf == "Train Conductor":
            description = f"congrats, {ctx.author.mention}! Your date for the day is {gf}!"
            footer = "Not too shabby..."
            cursor.execute("SELECT train_conductor FROM li_potential WHERE user_id=?", [uid])
            duplicate = cursor.fetchone()
            if duplicate[0] == "NONE":
                cursor.execute("UPDATE li_potential SET train_conductor='Train Conductor' WHERE user_id=?", [uid])
                field_name = "LI Potential"
                field_value = "The train conductor has joined the gang! Might consider asking her out? :wink:"
            else:
                field_name = "LI Potential"
                field_value = "It seems the train conductor is already part of the gang. Why don't you ask her out tho?"
            cursor.execute("UPDATE li_potential SET last_potential_li='train_conductor' WHERE user_id=?", [uid])

            if seconds is True:
                field_name = "Mr. Worldwide"
                field_value = "The train conductor twice in a row...\nYou must really travel **a lot**!"

        elif gf == "Fake Hiromi":
            title = f"congrats, {ctx.author.mention}! Your date for the day is {gf}!"
            footer = "Watashi o hitori ni shite kudasai..."

        embed = await self.helperClass.createEmbed(title=gf, text=description, footer=footer)

        embed.add_field(name=field_name, value=field_value, inline=True)
        embed.add_field(name=field2_name, value=field2_value, inline=True)

        image = discord.File(f"./gfGameImages/{gf}.png", filename="gf.png")
        embed.set_image(url="attachment://gf.png")
        await ctx.send(file=image, embed=embed)

        db.commit()
        cursor.close()

    async def displayLastGF(self, ctx, time):
        hours = int(time // 3600)
        minutes = int((time % 3600) // 60)
        seconds = int((time % 3600) % 60)
        description = f"You still have {hours}h {minutes} mins and {seconds}s until your next draw!"
        db = sqlite3.connect("main.sqlite")
        cursor = db.cursor()
        discordID = str(ctx.author.id)
        uid = await self.accountManager.getUserID(discordID=discordID, cursor=cursor)
        botSpamChannels = []
        botSpamChannels.append(self.client.get_channel(779873459756335104))
        channelId = ctx.channel

        if channelId not in botSpamChannels:
            embed = discord.Embed(title="Wrong channel!",
                                  description=f"Please take this to {botSpamChannels[0].mention}", color=0xFFA800)
            await ctx.send(embed=embed)
        else:
            checkUser = await self.accountManager.checkUser(discord_id=discordID, cursor=cursor)
            if checkUser == "register":
                embed = await self.helperClass.createEmbed(title=f"Error #404 - User {str(ctx.author)} not registered!",
                                                           text="Please register before playing! (-register)",
                                                           footer="Contact Eisritter#6969 if you encounter any issues!")
                await ctx.send(embed=embed)
            elif checkUser == "update":
                embed = await self.helperClass.createEmbed(title=f"Error - User {str(ctx.author)} is not up to date!",
                                                           text="Please update to the newest stand with -update!",
                                                           footer="Contact Eisritter#6969 if you encounter any issues!")
                await ctx.send(embed=embed)
            else:
                cursor.execute("SELECT last_gf FROM usersnew WHERE user_id=?", [uid])
                lastGf = cursor.fetchone()
                if lastGf is not None:
                    name = lastGf[0]
                else:
                    name = "None"

                if lastGf is not None:
                    title = "Slow down dude!"
                    field_name = lastGf[0]
                    field_value = f"The last pull you made was {field_name}"
                    footer = "retry later mate..."
                else:
                    title = "This is awkward..."
                    field_name = "Your last pull is... No one?"
                    field_value = "How could that happen..."
                    footer = "Might as well contact Eisritter#6969, sumn ain't right"

                embed = discord.Embed(title=title, description=description, color=0xFFA800)
                embed.set_footer(text=footer)

                embed.add_field(name=field_name, value=field_value, inline=True)

                image = discord.File(f"./gfGameImages/{name}.png", filename="gf.png")
                embed.set_image(url="attachment://gf.png")
                await ctx.send(file=image, embed=embed)

        cursor.close()

    @commands.command(aliases=["oialt gf", "gfo", "gf oialt"])
    @commands.cooldown(1, cooldowntime, commands.BucketType.user)
    async def ogf(self, ctx):
        db = sqlite3.connect("main.sqlite")
        cursor = db.cursor()
        botSpamChannels = []
        botSpamChannels.append(self.client.get_channel(779873459756335104))
        channelId = ctx.channel
        discordId = str(ctx.author.id)
        user_id = await self.accountManager.getUserID(discordId, cursor)

        if channelId not in botSpamChannels:
            embed = discord.Embed(title="Wrong channel!",
                                  description=f"Please take this to {botSpamChannels[0].mention}",
                                  color=HelperClass.orange)
            ctx.command.reset_cooldown(ctx)
            await ctx.send(embed=embed)
        else:
            checkUser = await self.accountManager.checkUser(discord_id=discordId, cursor=cursor)
            if checkUser == "register":
                embed = await self.helperClass.createEmbed(title=f"Error #404 - User {str(ctx.author)} not registered!",
                                                           text="Please register before playing! (-register)",
                                                           footer="Contact Eisritter#6969 if you encounter any issues!")
                await ctx.send(embed=embed)
                ctx.command.reset_cooldown(ctx)
            elif checkUser == "update":
                embed = await self.helperClass.createEmbed(title=f"Error - User {str(ctx.author)} is not up to date!",
                                                           text="Please update to the newest stand with -update!",
                                                           footer="Contact Eisritter#6969 if you encounter any issues!")
                await ctx.send(embed=embed)
                ctx.command.reset_cooldown(ctx)
            else:
                await self.chooseGF(ctx)
        cursor.close()

    @commands.command()
    async def oharem(self, ctx):
        db = sqlite3.connect("main.sqlite")
        cursor = db.cursor()
        discordID = str(ctx.author.id)
        uid = await self.accountManager.getUserID(discordID=discordID, cursor=cursor)
        user_name = str(ctx.author)
        count = 0

        checkUser = await self.accountManager.checkUser(discord_id=discordID, cursor=cursor)
        if checkUser == "register":
            embed = await HelperClass.createEmbed(title=f"Error #404 - User {str(ctx.author)} not registered!",
                                                  text="Please register before playing! (-register)",
                                                  footer="Contact Eisritter#6969 if you encounter any issues!")
            await ctx.send(embed=embed)
        elif checkUser == "update":
            embed = await HelperClass.createEmbed(title=f"Error - User {str(ctx.author)} is not up to date!",
                                                  text="Please update to the newest stand with -update!",
                                                  footer="Contact Eisritter#6969 if you encounter any issues!")
            await ctx.send(embed=embed)
        else:
            members = []
            cursor.execute(
                "SELECT judie, lauren, messy_hair_lauren, carla, iris, aiko, jasmine, rebecca FROM oialt_harem WHERE user_id=?",
                [uid])
            yesno = cursor.fetchone()
            for i in yesno:
                if i != 'NONE':
                    count = count + 1
                    members.append(i)
            for j in members:
                if j == "NONE":
                    members.remove(j)
                    count = count - 1

            haremlist = ", ".join(members)

            if haremlist == "":
                haremlist = "You haven't collected anyone for your harem yet..."

            embed_title = f"Harem of **{user_name[:-5]}**:"
            embed = discord.Embed(title=embed_title, description=haremlist, color=0xFFA800)
            embed.set_footer(text=f"Progress: {count} / 8")
            await ctx.send(embed=embed)

        cursor.close()

    @commands.command()
    async def stabbyclan(self, ctx):
        db = sqlite3.connect("main.sqlite")
        cursor = db.cursor()
        discordID = str(ctx.author.id)
        uid = await self.accountManager.getUserID(discordID=discordID, cursor=cursor)
        user_name = str(ctx.author)
        count = 0

        checkUser = await self.accountManager.checkUser(discord_id=discordID, cursor=cursor)
        if checkUser == "register":
            embed = await HelperClass.createEmbed(title=f"Error #404 - User {str(ctx.author)} not registered!",
                                                  text="Please register before playing! (-register)",
                                                  footer="Contact Eisritter#6969 if you encounter any issues!")
            await ctx.send(embed=embed)
        elif checkUser == "update":
            embed = await HelperClass.createEmbed(title=f"Error - User {str(ctx.author)} is not up to date!",
                                                  text="Please update to the newest stand with -update!",
                                                  footer="Contact Eisritter#6969 if you encounter any issues!")
            await ctx.send(embed=embed)
        else:
            members = []
            cursor.execute(
                "SELECT police, hitman, yakuza, priest, exterminator, anastasia FROM stabby_mikes WHERE user_id=?",
                [uid])
            yesno = cursor.fetchone()
            for i in yesno:
                if i != 'NONE':
                    count = count + 1
                    members.append(i)

            for j in members:
                if j == "NONE":
                    members.remove(j)
                    count = count - 1

            mikes = ", ".join(members)

            if mikes == "":
                mikes = "You haven't collected anyone for your clan yet..."

            embed_title = f"Stabby Clan of {user_name[:-5]}:"
            embed = discord.Embed(title=embed_title, description=mikes, color=0xFFA800)
            embed.set_footer(text=f"Progress: {count} / 6")
            await ctx.send(embed=embed)
        cursor.close()

    @commands.command()
    async def theboys(self, ctx):
        db = sqlite3.connect("main.sqlite")
        cursor = db.cursor()
        discordID = str(ctx.author.id)
        uid = await self.accountManager.getUserID(discordID=discordID, cursor=cursor)
        user_name = str(ctx.author)
        count = 0

        checkUser = await self.accountManager.checkUser(discord_id=discordID, cursor=cursor)
        if checkUser == "register":
            embed = await HelperClass.createEmbed(title=f"Error #404 - User {str(ctx.author)} not registered!",
                                                  text="Please register before playing! (-register)",
                                                  footer="Contact Eisritter#6969 if you encounter any issues!")
            await ctx.send(embed=embed)
        elif checkUser == "update":
            embed = await HelperClass.createEmbed(title=f"Error - User {str(ctx.author)} is not up to date!",
                                                  text="Please update to the newest stand with -update!",
                                                  footer="Contact Eisritter#6969 if you encounter any issues!")
            await ctx.send(embed=embed)
        else:
            members = []
            cursor.execute("SELECT mc, tom, oliver, fit_jack, asmodeus, hiromi FROM the_boys WHERE user_id=?",
                           [uid])
            yesno = cursor.fetchone()
            for i in yesno:
                if i != 'NONE':
                    count = count + 1
                    members.append(i)

            for j in members:
                if j == "NONE":
                    members.remove(j)
                    count = count - 1

            bois = ", ".join(members)

            if bois == "":
                bois = "You haven't collected anyone for your boys yet..."

            embed_title = f"The Boys of {user_name[:-5]}:"
            embed = discord.Embed(title=embed_title, description=bois, color=0xFFA800)
            embed.set_footer(text=f"Progress: {count} / 6")
            await ctx.send(embed=embed)
        cursor.close()

    @commands.command()
    async def potentialLis(self, ctx):
        db = sqlite3.connect("main.sqlite")
        cursor = db.cursor()
        discordID = str(ctx.author.id)
        uid = await self.accountManager.getUserID(discordID=discordID, cursor=cursor)
        user_name = str(ctx.author)
        count = 0

        checkUser = await self.accountManager.checkUser(discord_id=discordID, cursor=cursor)
        if checkUser == "register":
            embed = await HelperClass.createEmbed(title=f"Error #404 - User {str(ctx.author)} not registered!",
                                                  text="Please register before playing! (-register)",
                                                  footer="Contact Eisritter#6969 if you encounter any issues!")
            await ctx.send(embed=embed)
        elif checkUser == "update":
            embed = await HelperClass.createEmbed(title=f"Error - User {str(ctx.author)} is not up to date!",
                                                  text="Please update to the newest stand with -update!",
                                                  footer="Contact Eisritter#6969 if you encounter any issues!")
            await ctx.send(embed=embed)
        else:
            members = []
            cursor.execute(
                "SELECT ava, lilith, fit_jack_groupie, train_conductor, shop_girl, stone_elephant FROM li_potential WHERE user_id=?",
                [uid])
            yesno = cursor.fetchone()
            for i in yesno:
                if i != 'NONE':
                    count = count + 1
                    members.append(i)

            for j in members:
                if j == "NONE":
                    members.remove(j)
                    count = count - 1

            potentials = ", ".join(members)

            if potentials == "":
                potentials = "You haven't collected anyone for your potential LI's yet..."

            embed_title = f"Potential LI's of {user_name[:-5]}:"
            embed = discord.Embed(title=embed_title, description=potentials, color=0xFFA800)
            embed.set_footer(text=f"Progress: {count} / 6")
            await ctx.send(embed=embed)
        cursor.close()

    @commands.command(aliases=["protectorso", "oprotections", "oprotection", "protectionso", "protectiono"])
    async def oprotectors(self, ctx):
        db = sqlite3.connect("main.sqlite")
        cursor = db.cursor()
        discordID = str(ctx.author.id)
        user_name = str(ctx.author)

        # check if user registered & up to date
        checkUser = await self.accountManager.checkUser(discord_id=discordID, cursor=cursor)
        # if no build error embed
        if checkUser == "register":
            embed = await self.helperClass.createEmbed(title=f"Error #404 - User {str(ctx.author)} not registered!",
                                                       text="Please register before playing! (-register)",
                                                       footer="Contact Eisritter#6969 if you encounter any issues!")
            await ctx.send(embed=embed)
        elif checkUser == "update":
            embed = await self.helperClass.createEmbed(title=f"Error - User {str(ctx.author)} is not up to date!",
                                                       text="Please update to the newest stand with -update!",
                                                       footer="Contact Eisritter#6969 if you encounter any issues!")
            await ctx.send(embed=embed)
        # if yes:
        else:
            #   search thru 'eternum_harem' table for entries
            uid = await self.accountManager.getUserID(discordID=discordID, cursor=cursor)
            members = []
            cursor.execute("SELECT funtime, mc, aiko, nine_three FROM oialt WHERE user_id = ?", [uid])
            yesno = cursor.fetchone()
            harem = "**Harem:**\nFuntime: :x:"
            if yesno[0] == 1:
                harem = "**Harem:**\nFuntime: ✅"
            members.append(harem)

            mikes = "**Stabby Clan:**\nMC: :x:"
            if yesno[1] == 1:
                mikes = "**Stabby Clan:**\nMC: ✅"
            members.append(mikes)

            theboys = "**The Boys:**\nAiko: :x:"
            if yesno[2] == 1:
                theboys = "**The Boys:**\nAiko: ✅"
            members.append(theboys)

            potentialLis = "**Potential LI's:**\n93: :x:"
            if yesno[3] == 1:
                potentialLis = "**Potential LI's:**\n93: ✅"
            members.append(potentialLis)

            #   compile entries to list
            protectorlist = "\n".join(members)

            embed_title = f"OiaLt Protectors of **{user_name[:-5]}**:"
            #   build embed (color pink) with categories 'got x/y' + names & 'missing z/y' + names --> + emotes?
            embed = discord.Embed(title=embed_title, description=protectorlist, color=HelperClass.orange)
            await ctx.send(embed=embed)

    @commands.command(aliases=['oialt collections', 'collectionso', 'collections oialt'])
    async def oCollections(self, ctx):
        db = sqlite3.connect("main.sqlite")
        cursor = db.cursor()
        discordID = str(ctx.author.id)
        user_name = str(ctx.author)
        haremcount = 0
        homiecount = 0
        mikecount = 0
        potentialscount = 0
        # check if user registered & up to date
        checkUser = await self.accountManager.checkUser(discord_id=discordID, cursor=cursor)
        # if no build error embed
        if checkUser == "register":
            embed = await self.helperClass.createEmbed(title=f"Error #404 - User {str(ctx.author)} not registered!",
                                                       text="Please register before playing! (-register)",
                                                       footer="Contact Eisritter#6969 if you encounter any issues!")
            await ctx.send(embed=embed)
        elif checkUser == "update":
            embed = await self.helperClass.createEmbed(title=f"Error - User {str(ctx.author)} is not up to date!",
                                                       text="Please update to the newest stand with -update!",
                                                       footer="Contact Eisritter#6969 if you encounter any issues!")
            await ctx.send(embed=embed)
        # if yes:
        else:
            embed_title = f"OiaLt Collections of **{user_name[:-5]}**:"
            embed = discord.Embed(title=embed_title, color=HelperClass.orange)

            #   search thru tables for entries
            uid = await self.accountManager.getUserID(discordID=discordID, cursor=cursor)
            members = []

            # HAREM
            cursor.execute(
                "SELECT judie, lauren, messy_hair_lauren, carla, iris, aiko, jasmine, rebecca FROM oialt_harem WHERE user_id=?", [uid])
            yesno = cursor.fetchone()
            for i in yesno:
                if i != 'NONE':
                    haremcount = haremcount + 1
                    members.append(i)
            for j in members:
                if j == "NONE":
                    members.remove(j)
                    haremcount = haremcount - 1

            #   compile entries to list
            haremlist = "\n".join(members)

            if haremlist == "":
                haremlist = "You haven't collected anyone for your harem yet..."
            embed.add_field(name=f"Harem: ({haremcount}/8):", value=haremlist)

            members = []

            # THE BOYS
            cursor.execute(
                "SELECT mc, tom, oliver, fit_jack, asmodeus, hiromi FROM the_boys WHERE user_id=?", [uid])
            yesno = cursor.fetchone()
            for i in yesno:
                if i != 'NONE':
                    homiecount = homiecount + 1
                    members.append(i)
            for j in members:
                if j == "NONE":
                    members.remove(j)
                    homiecount = homiecount - 1

            #   compile entries to list
            homielist = "\n".join(members)

            if homielist == "":
                homielist = "You haven't collected any of the boys yet..."

            embed.add_field(name=f"The Boys: ({homiecount}/6):", value=homielist)

            members = []

            # STABBY CLAN
            cursor.execute(
                "SELECT police, hitman, yakuza, priest, exterminator, anastasia FROM stabby_mikes WHERE user_id=?",
                [uid])
            yesno = cursor.fetchone()
            for i in yesno:
                if i != 'NONE':
                    mikecount = mikecount + 1
                    members.append(i)
            for j in members:
                if j == "NONE":
                    members.remove(j)
                    mikecount = mikecount - 1

            # compile entries to list
            mikelist = "\n".join(members)

            if mikelist == "":
                mikelist = "You haven't collected any Mike yet..."
            embed.add_field(name=f"Stabby Clan: ({mikecount}/6):", value=mikelist)

            # POTENTIAL LI'S

            members = []

            cursor.execute(
                "SELECT ava, lilith, fit_jack_groupie, train_conductor, shop_girl, stone_elephant FROM li_potential WHERE user_id=?",
                [uid])
            yesno = cursor.fetchone()
            for i in yesno:
                if i != 'NONE':
                    potentialscount = potentialscount + 1
                    members.append(i)
            for j in members:
                if j == "NONE":
                    members.remove(j)
                    potentialscount = potentialscount - 1

            #   compile entries to list
            potentialslist = "\n".join(members)

            if potentialslist == "":
                potentialslist = "You haven't collected any of the potential LI's yet..."
            embed.add_field(name=f"Potential LI's: ({potentialscount}/6):", value=potentialslist)

            # Protectors

            members = []
            cursor.execute("SELECT funtime, mc, aiko, nine_three FROM oialt WHERE user_id = ?", [uid])
            yesno = cursor.fetchone()
            harem = "**Harem:**\nFuntime: :x:"
            if yesno[0] == 1:
                harem = "**Harem:**\nFuntime: ✅"
            members.append(harem)

            mikes = "**Stabby Clan:**\nMC: :x:"
            if yesno[1] == 1:
                mikes = "**Stabby Clan:**\nMC: ✅"
            members.append(mikes)

            theboys = "**The Boys:**\nAiko: :x:"
            if yesno[2] == 1:
                theboys = "**The Boys:**\nAiko: ✅"
            members.append(theboys)

            potentialLis = "**Potential LI's:**\n93: :x:"
            if yesno[3] == 1:
                potentialLis = "**Potential LI's:**\n93: ✅"
            members.append(potentialLis)

            #   compile entries to list
            protectorlist = "\n".join(members)

            embed.add_field(name="Protections:", value=protectorlist)

            await ctx.send(embed=embed)

    @ogf.error
    async def errorGF(self, ctx, error):
        if isinstance(error, commands.CommandOnCooldown):
            await self.displayLastGF(ctx, error.retry_after)


def setup(client):
    client.add_cog(OiaLt(client))
